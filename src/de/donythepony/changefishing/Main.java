package de.donythepony.changefishing;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin{
	
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(new ChangeFishingListener(),this);
		this.getCommand("changefishing").setExecutor(new CopyRight());
		System.out.println("[ChangeFishing] Starting up...");
		File f = new File(Main.getPlugin(Main.class).getDataFolder(), File.separator + "config.yml");
		if(!f.exists()){
			FileConfiguration data = YamlConfiguration.loadConfiguration(f);
			try {
				data.set("rare.1.Material", "261");
				data.set("rare.1.Rate", "1");
				data.set("rare.1.Name", "&5&lEpic Bow");
				data.set("rare.1.Lore1", "Hello darkness");
				data.set("rare.1.Lore2", "my");
				data.set("rare.1.Lore3", "old");
				data.set("rare.1.Lore4", "friend");
				data.set("rare.1.Enchantment", "DAMAGE_ALL:3,ARROW_INFINITE:1,KNOCKBACK:5");
				data.set("rare.1.OnlyGlow", "false");
				data.set("rare.1.min", "1");
				data.set("rare.1.max", "1");
				String[] x = {"/help","/help"};
				data.set("rare.1.commands", x);
				
				data.set("rare.2.Material", "264");
				data.set("rare.2.Rate", "5");
				data.set("rare.2.Name", "&b&lBest Diamond");
				data.set("rare.2.Lore1", "&3I");
				data.set("rare.2.Lore2", "&3love");
				data.set("rare.2.Lore3", "&3much!");
				data.set("rare.2.Lore4", "&3Diamonds!");
				data.set("rare.2.Enchantment", "DAMAGE_ALL:5");
				data.set("rare.2.OnlyGlow", "false");
				data.set("rare.2.min", "1");
				data.set("rare.2.max", "64");
				String[] y = {"/help","/help"};
				data.set("rare.2.commands", y);
				
				data.set("rare.3.Material", "322:1");
				data.set("rare.3.Rate", "11");
				data.set("rare.3.Name", "&6Fresh Eat");
				data.set("rare.3.Lore1", "");
				data.set("rare.3.Lore2", "");
				data.set("rare.3.Lore3", "");
				data.set("rare.3.Lore4", "");
				data.set("rare.3.Enchantment", "");
				data.set("rare.3.OnlyGlow", "true");
				data.set("rare.3.min", "5");
				data.set("rare.3.max", "15");
				String[] z = {"/help","/help"};
				data.set("rare.3.commands", z);				
				data.save(f);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onDisable(){

	}
}
