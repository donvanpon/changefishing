package de.donythepony.changefishing;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import net.md_5.bungee.api.ChatColor;

public class ChangeFishingListener implements Listener {
	
	@EventHandler
	public void onFishing(PlayerFishEvent event){
		try{
			Player player = event.getPlayer();
			Item e = (Item)event.getCaught();
			if(e != null){
				double d0 = player.getLocation().getX() - e.getLocation().getX();
				double d1 = player.getLocation().getY() - e.getLocation().getY();
				double d2 = player.getLocation().getZ() - e.getLocation().getZ();
				double d3 = (double) Math.sqrt(d0 * d0 + d1 * d1 + d2 * d2);
				double d4 = 0.1D;
				Vector newVel = new Vector(d0 * 0.1D,d1 * 0.1D + (double) Math.sqrt(d3) * 0.08D,d2 * 0.1D);
				e.setVelocity(newVel);
				e.setItemStack(getMaterial(player));
			}
		}catch(Exception ex){
			
		}
	}

	private ItemStack getMaterial(Player player) {
		LinkedList<LinkedList<String>> commandList = new LinkedList<>();
		Material mat = Material.APPLE;
		int min = 1;
		int max = 1;
		int matID = Integer.parseInt("1");
		byte byt = (byte)1;
		ItemStack drop = new ItemStack(mat);
		LinkedList<ItemStack> mats = new LinkedList<>();
		File f = new File(Main.getPlugin(Main.class).getDataFolder(), File.separator + "config.yml");
		if(f.exists()){
			FileConfiguration drops = YamlConfiguration.loadConfiguration(f);
			ConfigurationSection cS =  drops.getConfigurationSection("rare");
			for(String key : cS.getKeys(false)){
				int rate = 1;
				ItemStack stack = new ItemStack(Material.APPLE);
				try{
					rate = Integer.parseInt(cS.getString(key+".Rate"));
				} catch (Exception ex){
					ex.printStackTrace();
				}
				for(int i = 0; i < rate; i++){
					LinkedList<String> commandsToExecute = new LinkedList<>();
					String commandString = (cS.getString(key+".commands"));
					for(String c : commandString.split(",")){
						c = c.replace("[","");
						c = c.replace("]","");
						c = c.replaceAll(" ", "");
						c = c.replace("/", "");
						if(c != null && !"null".equals(c)){
							commandsToExecute.add(c);
						}
					}
					commandList.add(commandsToExecute);
					String matName = cS.getString(key+".Material");
					String[] matNames = matName.split(":");
					if(matNames.length == 2){

						try{
							matID = Integer.parseInt(matNames[0]);
							byt = (byte)(Integer.parseInt(matNames[1]));
						}catch(Exception ex){
							ex.printStackTrace();
						}
						stack = new ItemStack(matID,1,byt);
					} else {

							matID = Integer.parseInt("1");
							try{
								matID = Integer.parseInt(matNames[0]);
							}catch(Exception ex){
								ex.printStackTrace();
							}
							stack = new ItemStack(matID,1);
					}
						
					Material tempMat = stack.getData().getItemType();
					ItemStack item = stack;
					item.setType(tempMat);
					ItemMeta meta = item.getItemMeta();
					meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', cS.getString(key+".Name")));
					List<String> lore = new LinkedList<>();
					String tempLore = cS.getString(key+".Lore1");
					if(StringUtils.isNotBlank(tempLore)){
						lore.add(ChatColor.translateAlternateColorCodes('&',tempLore));						
					}
					tempLore = cS.getString(key+".Lore2");
					if(StringUtils.isNotBlank(tempLore)){
						lore.add(ChatColor.translateAlternateColorCodes('&',tempLore));						
					}
					tempLore = cS.getString(key+".Lore3");
					if(StringUtils.isNotBlank(tempLore)){
						lore.add(ChatColor.translateAlternateColorCodes('&',tempLore));						
					}
					tempLore = cS.getString(key+".Lore4");
					if(StringUtils.isNotBlank(tempLore)){
						lore.add(ChatColor.translateAlternateColorCodes('&',tempLore));						
					}
					
					meta.setLore(lore);
					String ench = cS.getString(key+".Enchantment");
					if(StringUtils.isNotBlank(ench)){
						for(String enchPlusLevel : Arrays.asList(ench.split(","))){
							String[] enchLevel = enchPlusLevel.split(":");
							int level;
							try{
								level = Integer.parseInt(enchLevel[1]);
							} catch(NumberFormatException ex){
								level = 1;
							}
								meta.addEnchant(Enchantment.getByName(enchLevel[0]), level, true);
						}
					}
					if(Boolean.valueOf(cS.getString(key+".OnlyGlow"))){
						meta.addEnchant(Enchantment.ARROW_FIRE, 5, false);
					}
					
					try{
						min = Integer.parseInt(cS.getString(key+".min"));
						max = Integer.parseInt(cS.getString(key+".max"));
					}catch(Exception ex){
						min = 1;
						max = 1;
					}
					Random ran = new Random();
					if(max > 1){
						if(!getChance(10f)){
							item.setAmount(min + ran.nextInt(max-min));											
						} else {
							item.setAmount(min);
						}
					}
					
					item.setItemMeta(meta);
					mats.add(item);
				}
			}
			Random ran = new Random();
			int ranNR = ran.nextInt(mats.size());
			LinkedList<String> com = commandList.get(ranNR);
			drop = mats.get(ranNR);
			for(String command : com){
				player.performCommand(command);
			}
		}
		return drop;
	}
	
	private boolean getChance(double percentage){
		percentage = percentage/100;
		if(percentage == 0){
			return false;
		}
		Random r = new Random();
		float chance = r.nextFloat();
		if (chance <= percentage){
			return true;
		} else {
			return false;
		}
	}
}
